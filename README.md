# chrome-base-extension

Place your js file into **ui/popup/js/menus/** folder. See the example file and enjoy !

## Available functions

Create button
```
Menu.builder.button(title, event);
```

Create menu
```
Menu.builder.menu(title);
```

Execute script on client page
```
Menu.executeScript(callbackExecute);
```

## Other informations
By default, return button is create. To override them, declare an button with name **return** like this
```
SomeApp.return = Menu.builder.button("Return Override", ()=>{});
```

See the license file