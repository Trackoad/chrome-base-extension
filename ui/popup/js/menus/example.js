const ExempleApp = {
  title:"Exemple"
};

ExempleApp.action = Menu.builder.button("Client Action Alert", 
  () => {
    Menu.executeScript(() => {
      alert("Client : Hello world !");
    });
  }
);

ExempleApp.submenu = Menu.builder.menu("SubMenu");

ExempleApp.submenu.subAction = Menu.builder.button("Client Console Action", 
() => {
  console.log("Extension : Hello world !"); 
  Menu.executeScript(() => {
    console.log("Client : Hello world !");
  });
}
);

Menu.addMenu(ExempleApp);
