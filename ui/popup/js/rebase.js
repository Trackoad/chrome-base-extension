Node.prototype.appendBuilder = function(element){
  this.append(element);
  return element;
};

Node.prototype.addButton = function(title, color, callback, menu){
  let button = document.createElement("button");
  button.style = `background-color:${color};`;
  button.onclick = callback;
  button.nextMenu = menu;
  let titleElement = document.createElement("span");
  titleElement.innerHTML = title;
  this.appendBuilder(button).appendBuilder(titleElement);
};

Node.prototype.clear = function(){
  this.innerHTML = "";
};
