const Menu = {
  elements:{
    start:true
  },
  buttonType:{
    FUNCTION:"orange",
    RETURN:"orange",
    MENU:"white"
  },
  baseElement:{},
  builder:{},
  returnMessage:"Return Menu",
  menusPath:"ui/popup/js/menus",
  clockWait:20
};

Menu.builder.menu = (title) => {
  return {
    title:title
  };
}

Menu.builder.button = (title, event) => {
  return {
    title:title,
    event:event
  }
};

Menu.buttonEvent = (mouseEvent) => {
  let button = mouseEvent.srcElement.parentElement;
  if(button.nextMenu){
    Menu.refresh(button.nextMenu);
  }
};

Menu.addButton = (title, type, callback = console.log, menu = undefined) => Menu.baseElement.addButton(title, type, callback, menu);

Menu.getScriptString = (callback) => {
  let str = callback.toString();
  return str.substring(str.indexOf("{") + 1, str.length - 1); 
};

Menu.executeScript = (callbackExecute) => chrome.tabs.executeScript(null, {code:Menu.getScriptString(callbackExecute)});

Menu.refresh = (start = Menu.elements) => {
  Menu.baseElement.clear();
  for(let key of Object.keys(start)){
    let element = start[key];

    if(typeof(element) !== 'object'){
      continue;
    }

    if(element.title && typeof(element.title) === 'string'){
      if(element.event && typeof(element.event) === 'function'){
        Menu.addButton(element.title, key.includes(Menu.buttonType.RETURN) ? Menu.buttonType.RETURN : Menu.buttonType.FUNCTION, element.event);
        continue;
      }

      Menu.addButton(element.title, Menu.buttonType.MENU, Menu.buttonEvent, element);
    }
  }
  if(start.return === undefined){

    start.return = Menu.builder.button(Menu.returnMessage, () => {
      Menu.refresh();
    });

    Menu.addButton(start.return.title, Menu.buttonType.RETURN, start.return.event);
  }
};

Menu.addMenu = (menu) => {
  if(!menu.title){
    console.error("init-error", menu);
    return;
  }
  Menu.elements[menu.title] = menu;
};

Menu.initFiles = (callback = console.log) => {
  let testedFunction = (dir) => {
    console.log(dir);
    let findElement = (pdir) => {
      pdir.createReader().readEntries((rr)=> {
        for(let subElement of rr){
          if(!subElement.isDirectory){
            continue;
          }
          if(subElement.fullPath.includes(Menu.menusPath)){
            Menu.menuDirectory = subElement;
            return;
          }
          findElement(subElement);
        }
      });
    };
    findElement(dir);
  };;
  
  chrome.runtime.getPackageDirectoryEntry(testedFunction);
  setTimeout(() => {
    callback(Menu.menuDirectory);
  }, 100);
};

Menu.addLocalScript = (fileName) => {
  document.body.appendBuilder(document.createElement("script")).src = fileName;
};

/*
  ==== MAIN ====
*/

Menu.main = () => {
  Menu.baseElement = document.body;
  Menu.initFiles((dir) => {
    dir.createReader().readEntries((fileList) => {
      for(let file of fileList){
        Menu.addLocalScript(`js/menus/${file.name}`);
      }
      Menu.elements["helpbro"] = true;
      setTimeout(() => {
        Menu.refresh();
      }, Menu.clockWait);
    });
  });
};

window.addEventListener("DOMContentLoaded", (event) => {
  Menu.main(event);
});

/*
  ==== END MAIN ====
*/